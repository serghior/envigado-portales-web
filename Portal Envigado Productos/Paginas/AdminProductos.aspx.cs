﻿using Jeduca.Clases.SQLDataUtils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_AdminProductos : SecuredPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(IsPostBack))
        {
            this.BindGridImages();
            this.BindGridProductos();
        }
    }

    private void BindGridImages()
    {
        try
        {
            string[] filePaths = Directory.GetFiles(Server.MapPath("/Galeria/"));
            List<ListItem> files = new List<ListItem>();
            foreach (string filePath in filePaths)
            {
                files.Add(new ListItem("/Galeria/" + Path.GetFileName(filePath), filePath));
            }
            listImages.DataSource = files;
            listImages.DataBind();
        }
        catch (Exception ex)
        {
            lblError.Text = "Error binding grid Images: " + ex.Message;
        }
    }
    protected void btnAddImage_Click(object sender, EventArgs e)
    {
        if (fileImagen.HasFile)
        {
            var name = Path.GetFileNameWithoutExtension(fileImagen.PostedFile.FileName);

            var data = fileImagen.FileBytes;
            if (SQLDataUtils.IsImage(fileImagen.PostedFile))
            {
                if (!(fileImagen.PostedFile.ContentLength > 3524288))
                {
                    string fileName = Path.GetFileName(fileImagen.PostedFile.FileName).ToLower();
                    fileImagen.PostedFile.SaveAs(Server.MapPath("/Galeria/") + fileName);
                    Response.Redirect(Request.Url.AbsoluteUri);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "alertify.alert('Tamaño máximo 3000kb.');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "alertify.alert('Sólo permitidos formatos de imagenes JPG o PNG.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "alertify.alert('No se ha especificado una imagen para actualizar.');", true);
        }
    }

    protected void listImagesServer_Delete(object sender, EventArgs e)
    {
        string filePath = (sender as LinkButton).CommandArgument;
        File.Delete(filePath);
        Response.Redirect(Request.Url.AbsoluteUri);
    }

    private void BindGridProductos()
    {
        try
        {
            var dtProductos = SQLDataUtils.SelectFromSQLTable("ConexionBD", "Catalogo", "ID,ImgUrl,Descripcion,NomItem,Activo", "", new Dictionary<string, object>());
            gvProductos.DataSource = dtProductos;
            gvProductos.DataBind();
        }
        catch (Exception ex)
        {
            lblError.Text = "Error binding grid Catalogo: " + ex.Message;
        }
    }

    protected void gvProductos_OnRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        pProductos.Visible = true;
        gvProductos.EditIndex = -1;
        this.BindGridProductos();
    }
    protected void gvProductos_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvProductos.EditIndex)
        {
            (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('¿Realmente desea eliminar este registro?');";
        }
    }
    protected void gvProductos_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int ID = Convert.ToInt32(gvProductos.DataKeys[e.RowIndex].Values[0]);
            SQLDataUtils.DeleteFromSQLTable("ConexionBD", "Catalogo", "ID=@ID", new Dictionary<string, object>() { { "@ID", ID } });
            this.BindGridProductos();
            pProductos.Visible = true;
        }
        catch (Exception ex)
        {
            lblError.Text = "Error deleting from grid Productos: " + ex.Message;
        }
    }
    protected void gvProductos_OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        pProductos.Visible = false;
        gvProductos.EditIndex = e.NewEditIndex;
        this.BindGridProductos();
    }
    protected void gvProductos_OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow row = gvProductos.Rows[e.RowIndex];
            int ID = Convert.ToInt32(gvProductos.DataKeys[e.RowIndex].Values[0]);
            string ImgUrl = (row.Cells[2].Controls[0] as TextBox).Text;
            string Descripcion = (row.Cells[3].Controls[0] as TextBox).Text;
            string NomItem = (row.Cells[4].Controls[0] as TextBox).Text;
            string Activo = (row.Cells[5].Controls[0] as TextBox).Text;

            SQLDataUtils.UpdateFromSQLTable("ConexionBD", "Catalogo", "ImgUrl=@ImgUrl,Descripcion=@Descripcion,NomItem=@NomItem,Activo=@Activo",
                "ID=@ID", new Dictionary<string, object>() { 
                {"@ID", ID}, 
                {"@ImgUrl", ImgUrl.Trim()},
                {"@Descripcion", Descripcion.Trim()},
                {"@NomItem", NomItem.Trim()},
                {"@Activo", Activo.Trim()}
            });
            gvProductos.EditIndex = -1;
            this.BindGridProductos();
            pProductos.Visible = true;
        }
        catch (Exception ex)
        {
            lblError.Text = "Error updating from grid Productos: " + ex.Message;
        }
    }
    protected void btnAddProductos_Click(object sender, EventArgs e)
    {
        try
        {
            SQLDataUtils.AddFromSQLTable("ConexionBD", "Catalogo", "ImgUrl,Descripcion,NomItem,Activo",
                "@ImgUrl,@Descripcion,@NomItem,@Activo",
                new Dictionary<string, object>() { 
                    { "@ImgUrl", txtImgUrl.Text.Trim() },
                    { "@Descripcion", txtDescripcion.Text.Trim() },
                    { "@NomItem", txtNomProd.Text.Trim() },
                    { "@Activo", rblActivo.SelectedValue }
                }, false);

            txtImgUrl.Text = "";
            txtDescripcion.Text = "";
            txtNomProd.Text = "";
            this.BindGridProductos();
            pProductos.Visible = true;
        }
        catch (Exception ex)
        {
            lblError.Text = "Error adding to grid Productos: " + ex.Message;
        }
    }
}