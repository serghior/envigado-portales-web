﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterFiles/MasterAdmin.master" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Paginas_Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        #contenido2 {
            text-align: center;
        }
        #ellogin > table{
            margin: 0 auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" Runat="Server">
    <div id="ellogin" style="margin-top:60px">
        <h1>Inicio de Sesión</h1>
        <asp:Login ID="loginSite" runat="server" DestinationPageUrl="~/Paginas/AdminHome.aspx" FailureText="Su identificación o contraseña no es correcto. Por favor inténtelo de nuevo." LoginButtonText="Entrar" PasswordLabelText="Contraseña:" PasswordRequiredErrorMessage="Contraseña requerida." RememberMeSet="True" RememberMeText="No cerrar sesión." RenderOuterTable="False" TextLayout="TextOnTop" TitleText="" UserNameLabelText="Nombre de Usuario:" UserNameRequiredErrorMessage="El nombre de usuario es requerido." OnAuthenticate="loginSite_Authenticate" VisibleWhenLoggedIn="False" DisplayRememberMe="False"></asp:Login>
    </div>
</asp:Content>

