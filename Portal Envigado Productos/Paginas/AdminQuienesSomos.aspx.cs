﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Jeduca.Clases.SQLDataUtils;
using System.Data;

public partial class Paginas_AdminQuienesSomos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(IsPostBack))
        {
            var dtQuien = SQLDataUtils.SelectFromSQLTable("ConexionBD", "QuienesSomos", "Pagina,Contenido", "", new Dictionary<string, object>());
            if (dtQuien != null)
            {
                foreach (DataRow row in dtQuien.Rows)
                {
                    switch (Convert.ToString(row["Pagina"]))
                    {
                        case "QuienesSomos":
                            ftxtQuienesSomos.Text = Convert.ToString(row["Contenido"]);
                            break;
                        case "Mision":
                            ftxtMision.Text = Convert.ToString(row["Contenido"]);
                            break;
                        case "Vision":
                            ftxtVision.Text = Convert.ToString(row["Contenido"]);
                            break;
                        case "Valores":
                            ftxtValores.Text = Convert.ToString(row["Contenido"]);
                            break;
                        case "CasosExito":
                            ftxtCasosExito.Text = Convert.ToString(row["Contenido"]);
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
    protected void btnQuienesSomos_Click(object sender, EventArgs e)
    {
        SQLDataUtils.AddOrUpdateFromSQLTable("ConexionBD", "QuienesSomos", "Pagina,Contenido", "'QuienesSomos',@Contenido", "Pagina='QuienesSomos',Contenido=@Contenido", "Pagina='QuienesSomos'", new Dictionary<string, object>()
        {
            { "@Contenido", ftxtQuienesSomos.Text }
        });
        Terminar("Página Quienes Somos");
    }
    protected void btnMision_Click(object sender, EventArgs e)
    {
        SQLDataUtils.AddOrUpdateFromSQLTable("ConexionBD", "QuienesSomos", "Pagina,Contenido", "'Mision',@Contenido", "Pagina='Mision',Contenido=@Contenido", "Pagina='Mision'", new Dictionary<string, object>()
        {
            { "@Contenido", ftxtMision.Text }
        });
        Terminar("Página Misión");
    }
    protected void btnVision_Click(object sender, EventArgs e)
    {
        SQLDataUtils.AddOrUpdateFromSQLTable("ConexionBD", "QuienesSomos", "Pagina,Contenido", "'Vision',@Contenido", "Pagina='Vision',Contenido=@Contenido", "Pagina='Vision'", new Dictionary<string, object>()
        {
            { "@Contenido", ftxtVision.Text }
        });
        Terminar("Página Visión");
    }
    protected void btnValores_Click(object sender, EventArgs e)
    {
        SQLDataUtils.AddOrUpdateFromSQLTable("ConexionBD", "QuienesSomos", "Pagina,Contenido", "'Valores',@Contenido", "Pagina='Valores',Contenido=@Contenido", "Pagina='Valores'", new Dictionary<string, object>()
        {
            { "@Contenido", ftxtValores.Text }
        });
        Terminar("Página Valores");
    }
    protected void btnCasosExito_Click(object sender, EventArgs e)
    {
        SQLDataUtils.AddOrUpdateFromSQLTable("ConexionBD", "QuienesSomos", "Pagina,Contenido", "'CasosExito',@Contenido", "Pagina='CasosExito',Contenido=@Contenido", "Pagina='CasosExito'", new Dictionary<string, object>()
        {
            { "@Contenido", ftxtCasosExito.Text }
        });
        Terminar("Página Casos de éxito");
    }

    protected void Terminar(string que)
    {
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "alertify.success('"+que+" actualizada correctamente.');", true);
    }
}