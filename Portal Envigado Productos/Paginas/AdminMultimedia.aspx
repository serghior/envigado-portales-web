﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterFiles/MasterAdmin.master" AutoEventWireup="true" CodeFile="AdminMultimedia.aspx.cs" Inherits="Paginas_AdminMultimedia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #contenido2 {
            text-align: center;
        }
    </style>
    <link href="../MasterFiles/css/formularios.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="Server">

    <!--alerta para confirmar una acción-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/start/jquery-ui.css" rel="stylesheet" type="text/css" />
    <!--referencia del alertify-->
    <script src="../MasterFiles/alertify/js/alertify.min.js"></script>
    <link href="../MasterFiles/alertify/css/alertify.core.css" rel="stylesheet" />
    <link href="../MasterFiles/alertify/css/alertify.bootstrap.css" rel="stylesheet" />
    <link href="../MasterFiles/alertify/css/alertify.default.css" rel="stylesheet" />

    <div class="ginfo">
        <div class="formdatos">
            <div class="formcontrols">
                <br />
                <div class="divmodulo">
                    <div class="rowcontrols2" style="text-align: center;">
                        <h1><strong>Administración Multimedia
                        </strong></h1>
                    </div>
                    <%-- Videos --%>

                    <div id="contenedor_slider1">
                        <div id="videos_boton">
                            <i class="icon-video"></i>
                            &nbsp;&nbsp;Videos
                        </div>
                        <br />
                        <div class="contenedorflex">
                            <div class="campo">
                                <div class="ellable">Url_Video</div>
                                <div class="elcontrol">
                                    <asp:TextBox ID="txturlvideo" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="campo">
                                <div class="ellable">Descripción del Video</div>
                                <div class="elcontrol">
                                    <asp:TextBox ID="txtmultivideo" runat="server" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="campo">
                                <div class="ellable">Aparece en Home?</div>
                                <div class="elcontrol">
                                    <asp:DropDownList ID="ddbhomevideo" runat="server">
                                        <asp:ListItem>No</asp:ListItem>
                                        <asp:ListItem>Si</asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="buttonblue">
                            <asp:Button ID="btnaddvideo" runat="server" Text="Agregar" OnClick="btnaddvideo_Click" />
                        </div>
                        <hr />
                        <div class="gridview">
                            <asp:GridView ID="gvideos" runat="server" AutoGenerateColumns="False" CellPadding="4" OnRowDeleting="OnRowDeleting" ForeColor="#333333">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <Columns>
                                    <asp:BoundField HeaderText="ID" DataField="ID" />
                                    <asp:HyperLinkField HeaderText="Video" DataTextField="Url_Multimedia" DataNavigateUrlFields="Url_Multimedia" Target="_blank" />
                                    <asp:BoundField HeaderText="Descripción" DataField="Descripcion_Multimedia" />
                                    <asp:BoundField HeaderText="Visible en Home" DataField="MultimediaEnHome" />

                                    <asp:CommandField DeleteText="Eliminar" ShowDeleteButton="True" />

                                </Columns>
                                <EditRowStyle BackColor="#999999" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />

                            </asp:GridView>
                        </div>

                    </div>
                    <br />

                    <%-- Imágenes --%>

                    <div id="contenedor_slider1">
                        <div id="videos_boton">
                            <i class="icon-fotografia"></i>
                            &nbsp;&nbsp;Galería
                        </div>
                        <br />
                        <div class="contenedorflex">
                            <div class="campo">
                                <div class="ellable">Seleccione la imagen</div>
                                <div class="elcontrol">
                                    <asp:FileUpload ID="file" runat="server" />
                                </div>
                            </div>
                            <div class="campo">
                                <div class="ellable">Descripción de la imagen</div>
                                <div class="elcontrol">
                                    <asp:TextBox ID="txtmultiimagen" runat="server" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <div class="campo">
                                <div class="ellable">Aparece en Home?</div>
                                <div class="elcontrol">
                                    <asp:DropDownList ID="ddphomeimagen" runat="server">
                                        <asp:ListItem>No</asp:ListItem>
                                        <asp:ListItem>Si</asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="buttonblue">
                            <asp:Button ID="btnadddimg" runat="server" Text="Agregar" OnClick="btnadddimg_Click" />
                        </div>

                        <hr />
                         <div class="gridview">
                            <asp:GridView ID="gimagen" runat="server" AutoGenerateColumns="False" CellPadding="4" OnRowDeleting="OnRowDeletingImagen" ForeColor="#333333">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <Columns>
                                    <asp:BoundField HeaderText="ID" DataField="ID" />
                                    <asp:ImageField DataImageUrlField="Url_Multimedia" HeaderText="Imagen">
                                        <ControlStyle Width="50px" />
                                    </asp:ImageField>
                                    <asp:BoundField HeaderText="Descripción" DataField="Descripcion_Multimedia" />
                                    <asp:BoundField HeaderText="Visible en Home" DataField="MultimediaEnHome" />
                                    <asp:CommandField DeleteText="Eliminar" ShowDeleteButton="True" />
                                </Columns>
                                <EditRowStyle BackColor="#999999" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />

                            </asp:GridView>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <asp:Label ID="lblAviso" runat="server"></asp:Label>


        <asp:Label ID="lblMsg2" runat="server"></asp:Label>


        <asp:Label ID="lblError" runat="server"></asp:Label>

    </div>
</asp:Content>

