﻿using Jeduca.Clases.SQLDataUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Vision : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(IsPostBack))
        {
            var dt = SQLDataUtils.SelectFromSQLTable("ConexionBD", "QuienesSomos", "Contenido", "Pagina='Vision'", new Dictionary<string, object>());

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    lblVision.Text = Convert.ToString(row["Contenido"]);
                }
            }
        }

    }
}