﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterFiles/PortalProductos.master" AutoEventWireup="true" CodeFile="Multimedia.aspx.cs" Inherits="Paginas_Multimedia" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" Runat="Server">

    <script>
        $(document).ready(function () {
            $(".fancybox-button").fancybox({
                prevEffect: 'none',
                nextEffect: 'none',
                closeBtn: false,
                helpers: {
                    title: { type: 'inside' },
                    buttons: {}
                }
            });           
        });
        function galeria() {
            $('#carousel2').elastislide();
            $('#carousel').elastislide();

        }
</script>
<div id="contenedor_sliders">

				

						<div id="contenedor_slider1">
							<div id="videos_boton"><i class="icon-video"></i> 
								&nbsp;&nbsp;Videos</div>
							<div class="container demo-1">
					            <div class="main">
									<!-- Elastislide Carousel -->
									<ul id="carousel" class="elastislide-list">
                                        <asp:Literal  ID="lblvideos"  runat="server"></asp:Literal>
									<!--	<li><a href="#"><iframe width="220.078" height="180.703" src="https://www.youtube.com/embed/-_icctfc9Kw?list=RDz071pmsu-ZU" frameborder="0" allowfullscreen></iframe></a></li>
										<li><a href="#"><iframe width="220.078" height="180.703" src="https://www.youtube.com/embed/7xM1lqtemCo" frameborder="0" allowfullscreen></iframe></a></li>
										<li><a href="#"><iframe width="220.078" height="180.703" src="https://www.youtube.com/embed/2O1JFSuthNY" frameborder="0" allowfullscreen></iframe></a></li>
										-->
									</ul>
									<!-- End Elastislide Carousel -->
								</div>
							</div>
						</div>
						
<!--						------------------------>
						
								
						<div id="contenedor_slider1">
							<div id="videos_boton"><i class="icon-fotografia"></i> 
								&nbsp;&nbsp;Galería</div>
							<div class="container demo-1">
					            <div class="main">
									<!-- Elastislide Carousel -->
									<ul id="carousel2" class="elastislide-list">
                                        <asp:Literal ID="lblimagenes" runat="server"></asp:Literal>									
									</ul>
									<!-- End Elastislide Carousel -->
								</div>
							</div>
						</div>

					</div>

<asp:Label ID="lblerror" runat="server" Text=""></asp:Label>
</asp:Content>

