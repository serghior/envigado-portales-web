﻿using Jeduca.Clases.SQLDataUtils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Paginas_AdminHome : SecuredPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            this.BindGridImages();

            var dtPres = SQLDataUtils.SelectFromSQLTable("ConexionBD", "Home", "ID,TextoHome", "ID=1", new Dictionary<string, object>());
            if (dtPres != null)
            {
                foreach (DataRow row in dtPres.Rows)
                {
                    ftxtPresentacion.Text = Convert.ToString(row["TextoHome"]);
                }
            }
        }
    }

    private void BindGridImages()
    {
        try
        {
            string[] filePaths = Directory.GetFiles(Server.MapPath("~/Images/"));
            List<ListItem> files = new List<ListItem>();
            foreach (string filePath in filePaths)
            {
                files.Add(new ListItem("/Images/" + Path.GetFileName(filePath), filePath));
            }
            listImages.DataSource = files;
            listImages.DataBind();
        }
        catch (Exception ex)
        {
            lblError.Text = "Error binding grid Images: " + ex.Message;
        }
    }

    protected void btnAddImage_Click(object sender, EventArgs e)
    {
        if (fileImagen.HasFile)
        {
            var name = Path.GetFileNameWithoutExtension(fileImagen.PostedFile.FileName);
            if ((name == "banner1") || (name == "banner2") || (name == "banner3"))
            {
                var data = fileImagen.FileBytes;
                if (SQLDataUtils.IsImage(fileImagen.PostedFile))
                {
                    if (!(fileImagen.PostedFile.ContentLength > 524288))
                    {
                        FileInfo FileInServerJPG = new FileInfo(Server.MapPath("~/Images/") + name + ".jpg");
                        FileInfo FileInServerPNG = new FileInfo(Server.MapPath("~/Images/") + name + ".png");
                        if (FileInServerJPG.Exists)
                        {
                            File.Delete(Server.MapPath("~/Images/") + name + ".jpg");
                        }
                        if (FileInServerPNG.Exists)
                        {
                            File.Delete(Server.MapPath("~/Images/") + name + ".png");
                        }

                        string fileName = Path.GetFileName(fileImagen.PostedFile.FileName).ToLower();
                        fileImagen.PostedFile.SaveAs(Server.MapPath("~/Images/") + fileName);
                        Response.Redirect(Request.Url.AbsoluteUri);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "alertify.alert('Tamaño máximo 500kb.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "alertify.alert('Sólo permitidos formatos de imagenes JPG o PNG.');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "alertify.alert('La imagen tiene que tener como nombre: banner1, banner2 o banner3.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "alertify.alert('No se ha especificado una imagen para actualizar.');", true);
        }
    }

    protected void listImagesServer_Delete(object sender, EventArgs e)
    {
        string filePath = (sender as LinkButton).CommandArgument;
        File.Delete(filePath);
        Response.Redirect(Request.Url.AbsoluteUri);
    }

    protected void btnguardar_Click(object sender, EventArgs e)
    {
        SQLDataUtils.AddOrUpdateFromSQLTable("ConexionBD", "Home", "TextoHome", "@TextoHome", "TextoHome=@TextoHome", "ID=1", new Dictionary<string, object>()
        {
            { "@TextoHome", ftxtPresentacion.Text }
        });
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "alertify.success('Presentación actualizada correctamente.');", true);
    }
}