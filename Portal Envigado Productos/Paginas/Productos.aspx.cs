﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Jeduca.Clases.SQLDataUtils;
using System.Data;

public partial class Paginas_Productos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(IsPostBack))
        {
            var dtCatalogo = SQLDataUtils.SelectFromSQLTable("ConexionBD", "Catalogo", "ID,ImgUrl,Descripcion,NomItem,Activo", "Activo='Si'", new Dictionary<string, object>());
            foreach (DataRow row in dtCatalogo.Rows)
            {
                ltlCatalogo.Text += "<div class='producto'><a class='fancybox-button' rel='fancybox-button' href='" + Convert.ToString(row["ImgUrl"]) + "' title='" + Convert.ToString(row["Descripcion"]) + "'><img src='" + Convert.ToString(row["ImgUrl"]) + "' alt='' /><p class='nom-producto'>" + Convert.ToString(row["NomItem"]) + "</p></a></div>";
            }
        }
    }
}