﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterFiles/PortalProductos.master" AutoEventWireup="true" CodeFile="Productos.aspx.cs" Inherits="Paginas_Productos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <script>
        $(document).ready(function () {
            $(".fancybox-button").fancybox({
                prevEffect: 'none',
                nextEffect: 'none',
                closeBtn: false,
                helpers: {
                    title: { type: 'inside' },
                    buttons: {}
                }
            });
        });
    </script>


    <style>

        a.fancybox-button {
		text-decoration: none;
		color: #000;
		font-weight: 600;
	    }

        .ui-tooltip, .arrow:after {
            background: #000000;
            border: 2px solid white;
        }

        .ui-tooltip {
            padding: 10px 20px;
            color: white;
            font: bold 14px "Helvetica Neue", Sans-Serif;
            text-transform: uppercase;
            box-shadow: 0 0 7px black;
        }

        .arrow {
            width: 70px;
            height: 16px;
            overflow: hidden;
            position: absolute;
            left: 50%;
            margin-left: -35px;
            bottom: -16px;
        }

            .arrow.top {
                top: -16px;
                bottom: auto;
            }

            .arrow.left {
                left: 20%;
            }

            .arrow:after {
                content: "";
                position: absolute;
                left: 20px;
                top: -20px;
                width: 25px;
                height: 25px;
                box-shadow: 6px 5px 9px -9px black;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }

            .arrow.top:after {
                bottom: -20px;
                top: auto;
            }

        .producto {
            display: inline-block;
            width: 32%;
            margin: 0.5%;
        }

            .producto a img {
                width: 100%;
                 
            }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="Server">
    <div class="contents" id="content_productos">
        <asp:Literal ID="ltlCatalogo" runat="server" />
    </div>
</asp:Content>

