$(document).ready(function() {
	var url = window.location.href;
	if(url.indexOf("Home") >= 0)
	{
		$( "#minicio" ).addClass( "active" );
		$( "#mqsomos" ).removeClass( "active" );
		$( "#mservicios" ).removeClass( "active" );
		$( "#mmultimedia" ).removeClass( "active" );
		$( "#mcontactenos" ).removeClass( "active" );
	}
	else if(url.indexOf("QuienesSomos") >= 0)
	{
		$( "#mqsomos" ).addClass( "active" );
		$( "#minicio" ).removeClass( "active" );
		$( "#mservicios" ).removeClass( "active" );
		$( "#mmultimedia" ).removeClass( "active" );
		$( "#mcontactenos" ).removeClass( "active" );
	}
	else if(url.indexOf("Servicios") >= 0)
	{
		$( "#mservicios" ).addClass( "active" );
		$( "#minicio" ).removeClass( "active" );
		$( "#mqsomos" ).removeClass( "active" );
		$( "#mmultimedia" ).removeClass( "active" );
		$( "#mcontactenos" ).removeClass( "active" );
	}
	else if(url.indexOf("Multimedia") >= 0)
	{
		$( "#mmultimedia" ).addClass( "active" );
		$( "#minicio" ).removeClass( "active" );
		$( "#mservicios" ).removeClass( "active" );
		$( "#mqsomos" ).removeClass( "active" );
		$( "#mcontactenos" ).removeClass( "active" );
	}
	else if(url.indexOf("Contactenos") >= 0)
	{
		$( "#mcontactenos" ).addClass( "active" );
		$( "#minicio" ).removeClass( "active" );
		$( "#mservicios" ).removeClass( "active" );
		$( "#mmultimedia" ).removeClass( "active" );
		$( "#mqsomos" ).removeClass( "active" );
	} 
		else if(url.indexOf("Mision") >= 0)
	{
		$( "#mqsomos" ).addClass( "active" );
		$( "#smmision" ).addClass( "active2" );
		$( "#minicio" ).removeClass( "active" );
		$( "#mservicios" ).removeClass( "active" );
		$( "#mmultimedia" ).removeClass( "active" );
		$( "#mcontactenos" ).removeClass( "active" );
	}
	else if(url.indexOf("Vision") >= 0)
	{
		$( "#mqsomos" ).addClass( "active" );
		$( "#smvision" ).addClass( "active2" );
		$( "#minicio" ).removeClass( "active" );
		$( "#mservicios" ).removeClass( "active" );
		$( "#mmultimedia" ).removeClass( "active" );
		$( "#mcontactenos" ).removeClass( "active" );
	}
	else if(url.indexOf("Valores") >= 0)
	{
		$( "#mqsomos" ).addClass( "active" );
		$( "#smvalores" ).addClass( "active2" );
		$( "#minicio" ).removeClass( "active" );
		$( "#mservicios" ).removeClass( "active" );
		$( "#mmultimedia" ).removeClass( "active" );
		$( "#mcontactenos" ).removeClass( "active" );
	}
	
		else if(url.indexOf("Casos") >= 0)
	{
		$( "#mqsomos" ).addClass( "active" );
		$( "#smcexitos" ).addClass( "active2" );
		$( "#minicio" ).removeClass( "active" );
		$( "#mservicios" ).removeClass( "active" );
		$( "#mmultimedia" ).removeClass( "active" );
		$( "#mcontactenos" ).removeClass( "active" );
	}




	
}); 

$(document).ready(function(){
    $('#selectme').on('click', function() {
        if ($('ul#options').is(':hidden')) {
           $('ul#options').show();
           $('#arrow').removeClass('down');
           $('#arrow').addClass('up');
        }
        else {
            $('ul#options').hide();
            $('#arrow').removeClass('up');
            $('#arrow').addClass('down');
        }       
    });
    
    $('#arrow').on('click', function() {
        if ($(this).hasClass('down')) {
           $('ul#options').show();
           $(this).removeClass('down');
           $(this).addClass('up');
        }
        else {
            $('ul#options').hide();
            $(this).removeClass('up');
            $(this).addClass('down');
        }       
    });

    $('#options li a').on('click', function() {
        var value = $(this).text();
        $('#selectme').text(value);
        $('ul#options').hide();
    });
    
    var parametro = window.location.href.split("#");
	if (parametro[1] != undefined){
		window.location.href = "#" + parametro[1];
	}
});

