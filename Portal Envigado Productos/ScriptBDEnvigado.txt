USE [PortalEnvigadoProductos]
GO
/****** Object:  Table [dbo].[Catalogo]    Script Date: 18/07/2015 7:08:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Catalogo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ImgUrl] [nvarchar](100) NULL,
	[Descripcion] [nvarchar](max) NULL,
	[NomItem] [nvarchar](100) NULL,
	[Activo] [nvarchar](2) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Contactenos]    Script Date: 18/07/2015 7:08:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contactenos](
	[Nombre] [nvarchar](50) NULL,
	[Correo] [nvarchar](50) NULL,
	[Asunto] [nvarchar](50) NULL,
	[Mensaje] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Home]    Script Date: 18/07/2015 7:08:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Home](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TextoHome] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Multimedia]    Script Date: 18/07/2015 7:08:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Multimedia](
	[Url_Multimedia] [nvarchar](100) NULL,
	[Descripcion_Multimedia] [nvarchar](200) NULL,
	[MultimediaEnHome] [nvarchar](50) NULL,
	[TipoMultimedia] [nvarchar](50) NULL,
	[ID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuienesSomos]    Script Date: 18/07/2015 7:08:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuienesSomos](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Pagina] [nvarchar](20) NULL,
	[Contenido] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 18/07/2015 7:08:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL
) ON [PRIMARY]

GO
