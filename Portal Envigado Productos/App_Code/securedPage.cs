﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// this page will be used as a basePage class for all pages that needs to be secured .
/// so if you want to make some pages secured , 
/// just let them inherit from this class instead of directly  inheriting from System.Web.UI.Page
/// </summary>
/// 
public class SecuredPage : System.Web.UI.Page
{

    protected string LoginUrl
    {
        get { return "~/Paginas/Login.aspx"; }
    }

    // return true if the current page is the Login Page .
    private bool IsLoginPage
    {
        get
        {
            return VirtualPathUtility.GetFileName(Request.Path).ToLower() ==
            VirtualPathUtility.GetFileName(LoginUrl.ToLower());
        }
    }

    // Property to get/set the UserName from/in the session
    private const string UserNameKey = "UserName";
    protected Users UserName
    {
        get
        {
            return ((Users)Session[UserNameKey]);
        }
        set
        {
            Session[UserNameKey] = (Users)value;
        }
    }

    protected string DefaultPage
    {
        get
        {
            return "/Paginas/AdminHome.aspx";
        }
    }
    protected void RequestLogin()
    {
        string CurrentUrl = Request.RawUrl;
        Response.Redirect(LoginUrl + "?ReturnUrl=" + Server.HtmlEncode(CurrentUrl));
    }

    // use this method to redirect the user after sucessfull login , 
    // this method will make sure that the user will get redirected to the original url  that was on .

    protected void RedirectFromLoginPage(string TargetUrl)
    {
        if (UserName != null)
        {
            if (Request.QueryString["ReturnUrl"] != null)
            {
                Response.Redirect(Request.QueryString["ReturnUrl"]);
            }
            else
                Response.Redirect(TargetUrl);
        }
    }

    // you can just call this method , it will automatically redirect to default page ,
    protected void RedirectFromLoginPage()
    {
        RedirectFromLoginPage(DefaultPage);
    }

    protected override void OnInit(EventArgs e)
    {
        // if the user is not logged in  , redirect  to Login Page
        if (UserName == null && !IsLoginPage)
            RequestLogin();
        // this needed to initialize its base page class
        base.OnInit(e);
    }
}