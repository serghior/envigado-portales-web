﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterFiles/PortalServicios.master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Paginas_Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" Runat="Server">
    
<!--INICIO DESCRIPCION-->
<div id="cont_descripcion">
    <hr class="bot_menu" id="prev_text" />
    <p id="descripcionhome">
        <asp:Label ID="lblDescpHome" runat="server" />		
    </p>
    <hr class="bot_menu" id="post_text" />
</div>
<!--FIN DESCRIPCION-->

<!--INICIO ZONA MULTIMEDIA-->
<div id="full-contenido">
    <div id="multimedia"><i id="multimedia2" class="icon-multimedia"></i>
    Multimedia
    </div>
    <div id="cont-multi">
        <asp:Literal id="lblmultimedia" runat="server" />
    </div>
</div>					
<!-- FIN ZONA MULTIMEDIA-->

<!--INICIO MODAL MULTIMEDIA HOME-->
<div id="content_modal"></div>
<!--FIN MODAL MULTIMEDIA HOME-->
    <asp:Label ID="lblerror" runat="server" Text=""></asp:Label>
</asp:Content>

