﻿using Jeduca.Clases.SQLDataUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_AdminMultimedia : SecuredPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) {
            try
            {
                traerimagenes();
                traervideos();
            }
            catch (Exception ex)
            {

                lblError.Text = ex.Message;
            }
            
        }
    }
    protected void btnaddvideo_Click(object sender, EventArgs e)
    {
        try
        {
            Dictionary<string, object> Datos = new Dictionary<string, object>();

            Datos.Add("@Url_Multimedia", txturlvideo.Text);
            Datos.Add("@Descripcion_Multimedia", txtmultivideo.Text);
            Datos.Add("@MultimediaEnHome", ddbhomevideo.SelectedValue);
            Datos.Add("@TipoMultimedia", "Video");

            SQLDataUtils.AddFromSQLTable(
                "ConexionBD",
                "Multimedia",
                "Url_Multimedia,Descripcion_Multimedia,MultimediaEnHome,TipoMultimedia",
                "@Url_Multimedia,@Descripcion_Multimedia,@MultimediaEnHome,@TipoMultimedia", Datos, false);

            traervideos();
            txturlvideo.Text = "";
            txtmultivideo.Text = "";
            ddbhomevideo.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
        


    }

    public void traervideos() {

        Dictionary<string, object> Datos = new Dictionary<string, object>();

            DataTable table= SQLDataUtils.SelectFromSQLTable(
            "ConexionBD",
            "Multimedia",
            "ID,Url_Multimedia,Descripcion_Multimedia,MultimediaEnHome,TipoMultimedia",
            "TipoMultimedia='Video'", Datos);

            gvideos.DataSource = table;
            gvideos.DataBind();


    }
    protected void OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        Dictionary<string, object> Datos = new Dictionary<string, object>();
       
        int idvideo = Convert.ToInt32(gvideos.Rows[e.RowIndex].Cells[0].Text);
        Datos.Add("@id", idvideo);
        SQLDataUtils.DeleteFromSQLTable(
            "ConexionBD",
            "Multimedia",
            "ID=@id", Datos);
        traervideos();
       
    }
    protected void OnRowDeletingImagen(object sender, GridViewDeleteEventArgs e)
    {
        Dictionary<string, object> Datos = new Dictionary<string, object>();

        int idimagen = Convert.ToInt32(gimagen.Rows[e.RowIndex].Cells[0].Text);
        Datos.Add("@id", idimagen);
        SQLDataUtils.DeleteFromSQLTable(
            "ConexionBD",
            "Multimedia",
            "ID=@id", Datos);
        traerimagenes();

    }
    public void traerimagenes() {
        Dictionary<string, object> Datos = new Dictionary<string, object>();

        DataTable table = SQLDataUtils.SelectFromSQLTable(
        "ConexionBD",
        "Multimedia",
        "ID,Url_Multimedia,Descripcion_Multimedia,MultimediaEnHome,TipoMultimedia",
        "TipoMultimedia='Imagen'", Datos);

        gimagen.DataSource = table;
        gimagen.DataBind();
    }
    protected void btnadddimg_Click(object sender, EventArgs e)
    {
        try
        {
            var data = file.FileBytes;
            if (SQLDataUtils.IsImage(file.PostedFile))
            {
                string fileName = Path.GetFileName(file.PostedFile.FileName).ToLower();
                file.PostedFile.SaveAs(Server.MapPath("~/Images_Multimedia/") + fileName);

                string urlimagen = "/Images_Multimedia/" + fileName;

                //Agrego a la base de datos

                Dictionary<string, object> Datos = new Dictionary<string, object>();

                Datos.Add("@Url_Multimedia", urlimagen);
                Datos.Add("@Descripcion_Multimedia", txtmultiimagen.Text);
                Datos.Add("@MultimediaEnHome", ddphomeimagen.SelectedValue);
                Datos.Add("@TipoMultimedia", "Imagen");

                SQLDataUtils.AddFromSQLTable(
                    "ConexionBD",
                    "Multimedia",
                    "Url_Multimedia,Descripcion_Multimedia,MultimediaEnHome,TipoMultimedia",
                    "@Url_Multimedia,@Descripcion_Multimedia,@MultimediaEnHome,@TipoMultimedia", Datos, false);

                traerimagenes();

                txtmultiimagen.Text = "";
                ddphomeimagen.SelectedIndex = 0;

            }
            else {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "alertify.error('Sólo permitidos formatos de imagenes JPG o PNG.');", true);
            }



        }
        catch (Exception ex)
        {
            
            lblError.Text=ex.Message;
        }

    }
}