﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterFiles/MasterAdmin.master" AutoEventWireup="true" CodeFile="AdminHome.aspx.cs" Inherits="Paginas_AdminHome" %>
<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
<%--        <!--alerta para confirmar una acción-->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
    <link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/start/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <script type="text/javascript">
        function ShowPopup() {
            $("#dialog").dialog({
                title: "Completado",
                width: 450,
                buttons: {
                    Ok: function () {
                        $(this).dialog('close');
                    }
                },
                modal: true
            });
        }

        function changedel() {
            $('#gvBanner a').html('Eliminar');
        }
        function changedel2() {
            $('#gvBanner a').html('Eliminar');
            alertify.alert('¡Tiene que seleccionar un archivo!');
        }
        function changedel3() {
            $('#gvBanner a').html('Eliminar');
            alertify.alert('¡Debe llenar la descripción!');
        }
        function changedel4() {
            $('#gvBanner a').html('Eliminar');
            alertify.alert('¡Datos actualizados con éxito!');
        }

        function changedel5() {
            $('#gvBanner a').html('Eliminar');
            alertify.alert('¡El nombre del archivo es incorrecto, debe de ser banner1, banner2 o banner3!');
        }


    </script>
    <style type="text/css">
        #contenido2 {
            text-align: center;
        }
        .controlscolumns3 > table{
            margin: 0 auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="Server">

    <div id="dialog" style="display: none">
        Se han Guardado los datos exitosamente.
    </div>

    <div class="ginfo">
        <div class="formdatos">
            <div class="formcontrols">
                <br />
                <div id="messages" runat="server"></div>
                <div class="divmodulo">
                    <div class="rowcontrols2" style="text-align: center;">
                        <h1><strong>Administración Home
                        </strong></h1>
                    </div>
                    <br />
                    <div class="rowcontrols">
                        <asp:FileUpload ID="fileImagen" runat="server" />&nbsp;<asp:Button ID="btnAddImage" Text="Agregar Imagen" runat="server" OnClick="btnAddImage_Click" CausesValidation="True" />
                        <br />
                        <br />
                        <asp:DataList ID="listImages" runat="server" HorizontalAlign="Center" SelectedIndex="0" ToolTip="Image list" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <ItemTemplate>
                                <div style="display: inline-block; margin: 20px;">
                                    <div>
                                        <a href="<%# Eval("Text") %>" target="_blank">
                                            <asp:Image ID="img" ImageUrl='<%# Eval("Text") %>' runat="server" ImageAlign="TextTop" Width="100px" />
                                        </a>
                                    </div>
                                    <div>
                                        <asp:LinkButton ID="lnkDelete" Text="Eliminar" CausesValidation="false" CommandArgument='<%# Eval("Value") %>' runat="server" OnClick="listImagesServer_Delete" />
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                    <hr />
                    <br />
                    <div class="controlscolumns3">
                        <h1>Texto de Presentación</h1>
                        <FTB:FreeTextBox ID="ftxtPresentacion" Language="es-ES" runat="server" ButtonSet="OfficeMac" EnableHtmlMode="False" ToolbarLayout="JustifyLeft,JustifyRight,JustifyCenter,JustifyFull;BulletedList,NumberedList,Indent,Outdent;CreateLink,Unlink,InsertImage,InsertRule|"></FTB:FreeTextBox>
                    </div>
                    <br />

                    <div class="rowcontrols">
                        <div class="buttonblue">
                            <asp:Button ID="btnguardar" runat="server" Text="Guardar" class="controlscolumns1" CssClass="buttonterminar" OnClick="btnguardar_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <asp:Label ID="lblAviso" runat="server"></asp:Label>


        <asp:Label ID="lblMsg2" runat="server"></asp:Label>


        <asp:Label ID="lblError" runat="server"></asp:Label>

    </div>

</asp:Content>

