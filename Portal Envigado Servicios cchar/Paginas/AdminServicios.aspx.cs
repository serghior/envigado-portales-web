﻿using Jeduca.Clases.SQLDataUtils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_AdminServicios : SecuredPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(IsPostBack))
        {
            this.BindGridImages();
            this.BindGridServicios();
        }
    }

    private void BindGridImages()
    {
        try
        {
            string[] filePaths = Directory.GetFiles(Server.MapPath("/Galeria/"));
            List<ListItem> files = new List<ListItem>();
            foreach (string filePath in filePaths)
            {
                files.Add(new ListItem("/Galeria/" + Path.GetFileName(filePath), filePath));
            }
            listImages.DataSource = files;
            listImages.DataBind();
        }
        catch (Exception ex)
        {
            lblError.Text = "Error binding grid Images: " + ex.Message;
        }
    }
    protected void btnAddImage_Click(object sender, EventArgs e)
    {
        if (fileImagen.HasFile)
        {
            var name = Path.GetFileNameWithoutExtension(fileImagen.PostedFile.FileName);

            var data = fileImagen.FileBytes;
            if (SQLDataUtils.IsImage(fileImagen.PostedFile))
            {
                if (!(fileImagen.PostedFile.ContentLength > 3524288))
                {
                    string fileName = Path.GetFileName(fileImagen.PostedFile.FileName).ToLower();
                    fileImagen.PostedFile.SaveAs(Server.MapPath("/Galeria/") + fileName);
                    Response.Redirect(Request.Url.AbsoluteUri);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "alertify.alert('Tamaño máximo 3000kb.');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "alertify.alert('Sólo permitidos formatos de imagenes JPG o PNG.');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "alertify.alert('No se ha especificado una imagen para actualizar.');", true);
        }
    }

    protected void listImagesServer_Delete(object sender, EventArgs e)
    {
        string filePath = (sender as LinkButton).CommandArgument;
        File.Delete(filePath);
        Response.Redirect(Request.Url.AbsoluteUri);
    }

    private void BindGridServicios()
    {
        try
        {
            var dtServicios = SQLDataUtils.SelectFromSQLTable("ConexionBD", "Catalogo", "ID,ImgUrl,Descripcion,NomItem,Activo", "", new Dictionary<string, object>());
            gvServicios.DataSource = dtServicios;
            gvServicios.DataBind();
        }
        catch (Exception ex)
        {
            lblError.Text = "Error binding grid Catalogo: " + ex.Message;
        }
    }

    protected void gvServicios_OnRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        pServicios.Visible = true;
        gvServicios.EditIndex = -1;
        this.BindGridServicios();
    }
    protected void gvServicios_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowIndex != gvServicios.EditIndex)
        {
            (e.Row.Cells[0].Controls[2] as LinkButton).Attributes["onclick"] = "return confirm('¿Realmente desea eliminar este registro?');";
        }
    }
    protected void gvServicios_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int ID = Convert.ToInt32(gvServicios.DataKeys[e.RowIndex].Values[0]);
            SQLDataUtils.DeleteFromSQLTable("ConexionBD", "Catalogo", "ID=@ID", new Dictionary<string, object>() { { "@ID", ID } });
            this.BindGridServicios();
            pServicios.Visible = true;
        }
        catch (Exception ex)
        {
            lblError.Text = "Error deleting from grid Servicios: " + ex.Message;
        }
    }
    protected void gvServicios_OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        pServicios.Visible = false;
        gvServicios.EditIndex = e.NewEditIndex;
        this.BindGridServicios();
    }
    protected void gvServicios_OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow row = gvServicios.Rows[e.RowIndex];
            int ID = Convert.ToInt32(gvServicios.DataKeys[e.RowIndex].Values[0]);
            string ImgUrl = (row.Cells[2].Controls[0] as TextBox).Text;
            string Descripcion = (row.Cells[3].Controls[0] as TextBox).Text;
            string NomItem = (row.Cells[4].Controls[0] as TextBox).Text;
            string Activo = (row.Cells[5].Controls[0] as TextBox).Text;

            SQLDataUtils.UpdateFromSQLTable("ConexionBD", "Catalogo", "ImgUrl=@ImgUrl,Descripcion=@Descripcion,NomItem=@NomItem,Activo=@Activo",
                "ID=@ID", new Dictionary<string, object>() { 
                {"@ID", ID}, 
                {"@ImgUrl", ImgUrl.Trim()},
                {"@Descripcion", Descripcion.Trim()},
                {"@NomItem", NomItem.Trim()},
                {"@Activo", Activo.Trim()}
            });
            gvServicios.EditIndex = -1;
            this.BindGridServicios();
            pServicios.Visible = true;
        }
        catch (Exception ex)
        {
            lblError.Text = "Error updating from grid Servicios: " + ex.Message;
        }
    }
    protected void btnAddServicios_Click(object sender, EventArgs e)
    {
        try
        {
            SQLDataUtils.AddFromSQLTable("ConexionBD", "Catalogo", "ImgUrl,Descripcion,NomItem,Activo",
                "@ImgUrl,@Descripcion,@NomItem,@Activo",
                new Dictionary<string, object>() { 
                    { "@ImgUrl", txtImgUrl.Text.Trim() },
                    { "@Descripcion", txtDescripcion.Text.Trim() },
                    { "@NomItem", txtNomProd.Text.Trim() },
                    { "@Activo", rblActivo.SelectedValue }
                }, false);

            txtImgUrl.Text = "";
            txtDescripcion.Text = "";
            txtNomProd.Text = "";
            this.BindGridServicios();
            pServicios.Visible = true;
        }
        catch (Exception ex)
        {
            lblError.Text = "Error adding to grid Servicios: " + ex.Message;
        }
    }

    protected void gvServicios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvServicios.PageIndex = e.NewPageIndex;
        BindGridServicios();
    }
}