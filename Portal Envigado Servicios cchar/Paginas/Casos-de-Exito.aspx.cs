﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Jeduca.Clases.SQLDataUtils;
using System.Data;

public partial class Paginas_Casos_de_Exito : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(IsPostBack))
        {
            var dt = SQLDataUtils.SelectFromSQLTable("ConexionBD", "QuienesSomos", "Contenido", "Pagina='CasosExito'", new Dictionary<string, object>());

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    lblCasosExito.Text = Convert.ToString(row["Contenido"]);
                }
            }
        }
    }
}