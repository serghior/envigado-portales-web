﻿using Jeduca.Clases.SQLDataUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Multimedia : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack) 
        {
            try
            {
                traerimagenes();
                traervideos();

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "script", "galeria();", true);
            }
            catch (Exception ex)
            {

                lblerror.Text = ex.Message;
            }
        
        
        
        }

    }

    public void traervideos()
    {
        Dictionary<string, object> Datos = new Dictionary<string, object>();

        DataTable table = SQLDataUtils.SelectFromSQLTable(
        "ConexionBD",
        "Multimedia",
        "ID,Url_Multimedia,Descripcion_Multimedia,MultimediaEnHome,TipoMultimedia",
        "TipoMultimedia='Video'", Datos);

        foreach (DataRow row in table.Rows) 
        {
            var urlposition = Convert.ToString(row["Url_Multimedia"]).Split('=');
            var url_real = "https://www.youtube.com/embed/"+urlposition[1];
            lblvideos.Text += "<li id='video-multimedia'><a href='#'><iframe width='100%' height='180.703' src='" + url_real + "' frameborder='0' allowfullscreen></iframe></a></li>";              
    
        }
    }

    public void traerimagenes()
    {
        Dictionary<string, object> Datos = new Dictionary<string, object>();

        DataTable table = SQLDataUtils.SelectFromSQLTable(
        "ConexionBD",
        "Multimedia",
        "ID,Url_Multimedia,Descripcion_Multimedia,MultimediaEnHome,TipoMultimedia",
        "TipoMultimedia='Imagen'", Datos);

        foreach (DataRow row in table.Rows)
        {
            lblimagenes.Text += "<li id='imagen-multimedia'><a class='fancybox-button' rel='fancybox-button' href='" + Convert.ToString(row["Url_Multimedia"]) + "' title='" + Convert.ToString(row["Descripcion_Multimedia"]) + "'><img class='img-min' src='" + Convert.ToString(row["Url_Multimedia"]) + "' alt=''/></a></li>";

        }
        

    
    }
}