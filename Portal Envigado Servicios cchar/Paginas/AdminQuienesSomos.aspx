﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterFiles/MasterAdmin.master" AutoEventWireup="true" CodeFile="AdminQuienesSomos.aspx.cs" Inherits="Paginas_AdminQuienesSomos" %>

<%@ Register Assembly="FreeTextBox" Namespace="FreeTextBoxControls" TagPrefix="FTB" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #contenido2 {
            text-align: center;
        }
        .rowcontrols > table {
            margin: 0 auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="Server">
    <div class="ginfo">
        <div class="formdatos">
            <div class="formcontrols">
                <br />
                <div class="divmodulo">
                    <div class="rowcontrols2" style="text-align: center;">
                        <h1><strong>Quiénes Somos</strong></h1>
                    </div>
                    <div class="rowcontrols">
                        <FTB:FreeTextBox ID="ftxtQuienesSomos" Language="es-ES" runat="server" ButtonSet="OfficeMac" EnableHtmlMode="False" ToolbarLayout="JustifyLeft,JustifyRight,JustifyCenter,JustifyFull;BulletedList,NumberedList,Indent,Outdent;CreateLink,Unlink,InsertImage,InsertRule|"></FTB:FreeTextBox>
                    </div>
                    <asp:Button ID="btnQuienesSomos" Text="Guardar" runat="server" OnClick="btnQuienesSomos_Click" />
                </div>
                <div class="divmodulo">
                    <div class="rowcontrols2" style="text-align: center;">
                        <h1><strong>Misión</strong></h1>
                    </div>
                    <div class="rowcontrols">
                        <FTB:FreeTextBox ID="ftxtMision" Language="es-ES" runat="server" ButtonSet="OfficeMac" EnableHtmlMode="False" ToolbarLayout="JustifyLeft,JustifyRight,JustifyCenter,JustifyFull;BulletedList,NumberedList,Indent,Outdent;CreateLink,Unlink,InsertImage,InsertRule|"></FTB:FreeTextBox>
                    </div>
                    <asp:Button ID="btnMision" Text="Guardar" runat="server" OnClick="btnMision_Click" />
                </div>
                <div class="divmodulo">
                    <div class="rowcontrols2" style="text-align: center;">
                        <h1><strong>Visión</strong></h1>
                    </div>
                    <div class="rowcontrols">
                        <FTB:FreeTextBox ID="ftxtVision" Language="es-ES" runat="server" ButtonSet="OfficeMac" EnableHtmlMode="False" ToolbarLayout="JustifyLeft,JustifyRight,JustifyCenter,JustifyFull;BulletedList,NumberedList,Indent,Outdent;CreateLink,Unlink,InsertImage,InsertRule|"></FTB:FreeTextBox>
                    </div>
                    <asp:Button ID="btnVision" Text="Guardar" runat="server" OnClick="btnVision_Click" style="height: 26px" />
                </div>
                <div class="divmodulo">
                    <div class="rowcontrols2" style="text-align: center;">
                        <h1><strong>Valores</strong></h1>
                    </div>
                    <div class="rowcontrols">
                        <FTB:FreeTextBox ID="ftxtValores" Language="es-ES" runat="server" ButtonSet="OfficeMac" EnableHtmlMode="False" ToolbarLayout="JustifyLeft,JustifyRight,JustifyCenter,JustifyFull;BulletedList,NumberedList,Indent,Outdent;CreateLink,Unlink,InsertImage,InsertRule|"></FTB:FreeTextBox>
                    </div>
                    <asp:Button ID="btnValores" Text="Guardar" runat="server" OnClick="btnValores_Click" />
                </div>
                <div class="divmodulo">
                    <div class="rowcontrols2" style="text-align: center;">
                        <h1><strong>Casos de éxito</strong></h1>
                    </div>
                    <div class="rowcontrols">
                        <FTB:FreeTextBox ID="ftxtCasosExito" Language="es-ES" runat="server" ButtonSet="OfficeMac" EnableHtmlMode="False" ToolbarLayout="JustifyLeft,JustifyRight,JustifyCenter,JustifyFull;BulletedList,NumberedList,Indent,Outdent;CreateLink,Unlink,InsertImage,InsertRule|"></FTB:FreeTextBox>
                    </div>
                    <asp:Button ID="btnCasosExito" Text="Guardar" runat="server" OnClick="btnCasosExito_Click" />
                </div>
            </div>
        </div>
    </div>


</asp:Content>

