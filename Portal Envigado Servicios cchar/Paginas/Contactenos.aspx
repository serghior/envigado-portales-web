﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterFiles/PortalServicios.master" AutoEventWireup="true" CodeFile="Contactenos.aspx.cs" Inherits="Paginas_Contactenos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" Runat="Server">

<%--<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<script src="<%= SPContext.Current.Web.Url %>/_vti_bin/jquery.ui.timepicker.js"></script>
<link rel="stylesheet" href="<%= SPContext.Current.Web.Url %>/_vti_bin/jquery.ui.timepicker.css">
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>--%>

    <link href="../MasterFiles/css/contactenos.css" rel="stylesheet" />

<script type="text/javascript">
    function ValidaSoloNumeros() {
        //Numeros, 45 (-), 46(.) 
        if ((event.keyCode < 45) || (event.keyCode > 57) || (event.keyCode == 47)) {
            event.returnValue = false;
        }

    }
</script>
<script type="text/javascript">
    function showMessage(title, message, type, id) {

        var messages = id; //'<%= messages.ClientID %>';

        var imgClose = $("<img/>").attr({
            "src": "../MasterFiles/img/close-btn.png",
            "width": "18",
            "height": "18",
            "alt": "botón cerrar",
            "title": "cerrar",
            "class": "btn-close-message"
        }).click(function (e) {
            $(this).parent().fadeOut('slow', function () { $(this).remove(); });
            e.preventDefault();
        });

        if (type == 'error' && $("#" + messages).children().length > 0) {
            $("#" + messages + " > div:not(:first-child)").remove();
            $("#" + messages + " > div").attr("class", "box-" + type + " box-message").html("<strong>" + title + "</strong> " + message).append(imgClose);
        }
        else {
            var newMsg = $("<div></div>").attr("class", "box-" + type + " box-message").html("<strong>" + title + "</strong> " + message)

            imgClose.appendTo(newMsg);
            newMsg.appendTo($("#" + messages));

            newMsg.fadeIn('slow');
        }
    }

    function clearMessages() {

        var messages = '<%= messages.ClientID %>';
        $("#" + messages).empty();
    }

    function isPageValid() {

        if (typeof (Page_ClientValidate) == 'function') {
            Page_ClientValidate();

            if (Page_IsValid) {
                return true;
            }
            else {
                return false;
            }
        }


    }
</script>

<div class="ginfo">
    <div class="formdatos">
        <div class="formcontrols">
         
            
            
            <div id="messages" runat="server"></div>
            <br />
            <div class="rowcontrols">


                <div class="column1">
                    <div class="formtitulo">
                        Contáctenos
                    </div>
                      <br />
                    <div id="info_contacto1" class="info_contacto">
                        <asp:Label ID="lblDireccion" runat="server" Text="Carrera 79 # 48-83"></asp:Label>                  
                    </div>
                      <div id="info_contacto2" class="info_contacto">
                      <asp:Label ID="lblCiudad" runat="server" Text="Medellín-Colombia"></asp:Label>                  
                    </div>
                      <div id="info_contacto3" class="info_contacto">
                      <asp:Label  ID="lblTelefono" runat="server" Text="Tel: (57) 4-5813033"></asp:Label>                 
                    </div>
                      <div id="info_contacto4" class="info_contacto">
                   <asp:Label  ID="lblMovil" runat="server" Text="Movil: 3103706363"></asp:Label>
                      </div>
                     <div id="info_contacto5" class="info_contacto">
                     <asp:Label  ID="Label1" runat="server" Text="jeduca@jeduca.com"></asp:Label>

                     </div>
               
                </div>

                <div class="column2">
                    <div class="controles">
                        <div class="controlscolumns1">Nombre: </div>
                    <asp:TextBox ID="txtNombre" runat="server" CssClass="textboxes" > </asp:TextBox>
                    </div>

                       <div class="controles">
                        <div class="controlscolumns1">Correo: </div>
                    <asp:TextBox ID="txtCorreo" runat="server" CssClass="textboxes"  TextMode="Email"> </asp:TextBox>
                    </div>

                       <div class="controles">
                        <div class="controlscolumns1">Asunto: </div>
                    <asp:TextBox ID="txtAsunto" runat="server" CssClass="textboxes" > </asp:TextBox>
                    </div>

                    <div class="controles">
                        <div class="controlscolumns1">Mensaje: </div>
                    <asp:TextBox ID="txtMensaje" runat="server" CssClass="textMultiline" TextMode="MultiLine"  ></asp:TextBox>
                    </div>
                </div>

            </div>
            
            <br />
            <div class="rowcontrols">
                <div class="buttonblue">
                    <asp:Button Text="Enviar" ID="btnEnviar" runat="server" CssClass="buttonterminar" OnClick="btnEnviar_Click" />
                </div>
            </div>
           <iframe width="100%" height="250" frameborder="0" style="border:0"
src="https://www.google.com/maps/embed/v1/place?q=wakamole%20Nacho%20%26%20burger%20carrera%2030%20%23%2039bsur%2025%20int.%20101%20medellin%2C%20antioquia&key=AIzaSyCL0vvcPLiFdSaFrehTG9P4JUB4mM1Wm5Y" allowfullscreen></iframe>
        </div>
    </div>
</div>

<asp:Label ID="lblerror" runat="server"></asp:Label>
</asp:Content>

