﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Jeduca.Clases.SQLDataUtils;
using System.Data;

public partial class Paginas_Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(IsPostBack))
        {
            var dtPres = SQLDataUtils.SelectFromSQLTable("ConexionBD", "Home", "ID,TextoHome", "ID=1", new Dictionary<string, object>());
            if (dtPres != null)
            {
                foreach (DataRow row in dtPres.Rows)
                {
                    lblDescpHome.Text = Convert.ToString(row["TextoHome"]);
                }
            }
            traermultimedia();
        }
    }

    public void traermultimedia()
    {
        try
        {
            int contador_multimedia = 0;
            Dictionary<string, object> Datos = new Dictionary<string, object>();

            DataTable table = SQLDataUtils.SelectQueryFromSQLTable(
            "ConexionBD",
            @"SELECT        TOP (3) Url_Multimedia, Descripcion_Multimedia, MultimediaEnHome, TipoMultimedia, ID
            FROM            Multimedia where MultimediaEnHome='Si'
            ORDER BY ID DESC",
            Datos);

            foreach (DataRow row in table.Rows)
            {

                if (Convert.ToString(row["TipoMultimedia"]) == "Video")
                {
                    contador_multimedia += 1;
                    var urlposition = Convert.ToString(row["Url_Multimedia"]).Split('=');
                    var url_real = "https://www.youtube.com/embed/" + urlposition[1];

                    lblmultimedia.Text += "<div class='contenedor' id='contenedor" + contador_multimedia + @"'>
                    <iframe width='100%' height='200' src='"+url_real+"' frameborder='0' allowfullscreen></iframe><p id='desc_multim'>"+Convert.ToString(row["Descripcion_Multimedia"])+@"</p>
                    </div>";
                }
                else if (Convert.ToString(row["TipoMultimedia"]) == "Imagen")
                {
                    contador_multimedia += 1;
                    lblmultimedia.Text += "<div class='contenedor' id='contenedor" + contador_multimedia + @"'>
                    <a class='fancybox-button' rel='fancybox-button' href='" + Convert.ToString(row["Url_Multimedia"]) + "' title='" + Convert.ToString(row["Descripcion_Multimedia"]) + "'><img class='img-min' src='" + Convert.ToString(row["Url_Multimedia"]) + "' alt='' /></a><p class='descrip-exterior'>" + Convert.ToString(row["Descripcion_Multimedia"]) + @"</p>
                    </div>";

                }




            }


        }
        catch (Exception ex)
        {

            lblerror.Text = ex.Message;
        }
    }
}