﻿using Jeduca.Clases.SQLDataUtils;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Login : SecuredPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void loginSite_Authenticate(object sender, AuthenticateEventArgs e)
    {
        //Verificando si los datos son correctos
        string userNa = loginSite.UserName;
        string pass = loginSite.Password;

        Users result = UserLogin(userNa, pass);
        if (result != null)
        {
            // store the user name in the session
            Page.Session["UserName"] = result;
            // use this method instead of directly calling response.redirect ,
            // because this method will remember the previous page that the user requested ,

            RedirectFromLoginPage();
            e.Authenticated = true;
        }
        else
        {
            e.Authenticated = false;
        }
    }
    private Users UserLogin(string userName, string password)
    {
        var dtUsers = SQLDataUtils.SelectFromSQLTable("ConexionBD", "Usuarios", "UserName,Password",
            "UserName=@Name AND Password=@Pass", new Dictionary<string, object>(){
                {"@Name", userName },
                {"@Pass", password }
            });
        if (dtUsers != null)
        {
            foreach (DataRow row in dtUsers.Rows)
            {
                return new Users(Convert.ToString(row["UserName"]),
                    Convert.ToString(row["Password"]));
            }
        }
        else
        {
            //invalid user/password , return false 
            return null;
        }
        return null;
    }
}