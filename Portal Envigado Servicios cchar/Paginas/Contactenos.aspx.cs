﻿using Jeduca.Clases.SQLDataUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Paginas_Contactenos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnEnviar_Click(object sender, EventArgs e)
    {
        try
        {
            if (txtNombre.Text == string.Empty)
            {
                ShowErrorMessage("Validación: ", "El nombre es un campo obligatorio.");
                return;
            }
            if (txtCorreo.Text == string.Empty)
            {
                ShowErrorMessage("Validación: ", "El correo electrónico es un campo obligatorio.");
                return;
            }
            if (txtAsunto.Text == string.Empty)
            {
                ShowErrorMessage("Validación: ", "El asunto es un campo obligatorio.");
                return;
            }
            if (txtMensaje.Text == string.Empty)
            {
                ShowErrorMessage("Validación: ", "El mensaje es un campo obligatorio.");
                return;
            }

            //Visual, envíe un correo al usuario.
            SQLDataUtils.SendEmail(txtCorreo.Text,"riosserghio@gmail.com", txtAsunto.Text, @"
                <p>*Mensaje enviado automaticamente*</p>
	            <div id='body' style='color:white; font-family:arial;'>
		            <div style='width:100%; background-color:black;'><p style='padding: 10px;font-weight:bold;'>La Botter</p></div>
		            <div>
			            <div style='color:black; padding-left:10px'>Nombre de Contacto: " + txtNombre.Text + @"</div>
		            </div>
                    <br />
                    <div>
			            <div style='color:black; padding-left:10px'>Correo de Contacto: " + txtCorreo.Text + @"</div>
		            </div>
                    
                    <div>
			            <p style='padding: 10px; color:black;'>" + txtMensaje.Text + @"</p>
		            </div>
		            <div style='width:100%; text-align:center; background-color:black;'><p style='padding: 10px; font-size:0.8em;'>La Botter - Copyright © Todos los derechos reservados. 
                    Calle 41 a sur # 42 a 12 || Envigado-Ant || Barrio Vista Alegre 
                    Proyecto financiado por la Secretaría de Desarrollo Económico del Municipio de Envigado</p></div>
	            </div>");

            txtNombre.Text = "";
            txtAsunto.Text = "";
            txtCorreo.Text = "";
            txtMensaje.Text = "";
            ShowSuccessMessage("Enviado: ", "Su correo ha sido enviado al administrador de la página.");


        }
        catch (Exception ex)
        {
            lblerror.Text = ex.Message;
        }

    }

    public void ShowErrorMessage(string title, string message)
    {
        ShowMessage(title, message, MessageControlType.error);
    }

    public void ShowSuccessMessage(string title, string message)
    {
        ShowMessage(title, message, MessageControlType.success);
    }

    public void ShowInfoMessage(string title, string message)
    {
        ShowMessage(title, message, MessageControlType.information);
    }

    private void ShowMessage(string title, string message, MessageControlType type)
    {
        var id = this.messages.ClientID;

        ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "show_message" + Guid.NewGuid(), "showMessage('" + title.Replace("'", "\\'") + "','" + message.Replace("'", "\\'") + "', '" + type + "','" + id + "');", true);
    }

    public enum MessageControlType
    {
        error,
        success,
        information
    }
}