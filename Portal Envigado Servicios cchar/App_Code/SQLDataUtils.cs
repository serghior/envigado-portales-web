﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Librería utilizada para consultar, y traer datos de una forma más rápida y segura de SQLServer
/// </summary>
/// 

namespace Jeduca.Clases.SQLDataUtils
{
    public class SQLDataUtils
    {
        //Configuración webconfig para aceptar conexionstrings de ahí y especificar varias:
        //<connectionStrings>
        //    <add name="ConnectionStringName" connectionString="Data Source=JEDUCASERVER;Initial Catalog=DannBD;Persist Security Info=True;User ID=Leon_Ceballos;Password=Jeduca2015*" providerName="System.Data.SqlClient" />
        //  </connectionStrings>

        public static void AddFromSQLTable(string ConnectionStringName, string table, string fields, string valuesClause, Dictionary<string, object> values, bool multiple)
        {
            //If theres sth empty
            if (string.IsNullOrEmpty(ConnectionStringName) || string.IsNullOrEmpty(table) || string.IsNullOrEmpty(fields) || string.IsNullOrEmpty(valuesClause) || values == null)
                return;

            //Conexión ADD SQL 
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString))
            {
                valuesClause = multiple ? valuesClause : "(" + valuesClause + ")";
                //Setting SQL command and getting connection
                SqlCommand cmd = new SqlCommand("INSERT INTO " + table + " (" + fields + ") VALUES " + valuesClause);
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;
                //foreach key, add parammeter
                foreach (var key in values.Keys)
                {
                    cmd.Parameters.AddWithValue(key, values[key]);
                }
                connection.Open();
                cmd.ExecuteNonQuery();
            }

        }

        public static void AddOrUpdateFromSQLTable(string ConnectionStringName, string table, string fields, string valuesClause, string setClause, string whereClause, Dictionary<string, object> values)
        {
            //If theres sth empty
            if (string.IsNullOrEmpty(ConnectionStringName) || string.IsNullOrEmpty(table) || string.IsNullOrEmpty(fields) || string.IsNullOrEmpty(valuesClause)|| string.IsNullOrEmpty(setClause) || string.IsNullOrEmpty(whereClause) || values == null)
                return;

            //Select
            var entro = false;
            var dt = SelectFromSQLTable(ConnectionStringName, table, fields, whereClause, values);
            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    //Theres sth
                    entro = true;
                }
            }
            if (entro)
            {
                //Update 'coz theres sth
                UpdateFromSQLTable(ConnectionStringName, table, setClause, whereClause, values);
            }
            else
            {
                //Add 'coz theres not anything
                AddFromSQLTable(ConnectionStringName, table, fields, valuesClause, values, false);
            }
        }

        public static DataTable SelectFromSQLTable(string ConnectionStringName, string table, string fields, string whereClause, Dictionary<string, object> parameters)
        {
            //If theres sth empty
            if (string.IsNullOrEmpty(ConnectionStringName) || string.IsNullOrEmpty(table) || string.IsNullOrEmpty(fields))
                return null;

            //If theres where, add WHERE clause
            var WHERE = (string.IsNullOrEmpty(whereClause)) ? ("") : ("WHERE " + whereClause);

            // read the coonection string from web.config 
            //Conexión SQL UPDATE USER
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString))
            {
                //' declare the command that will be used to execute the select statement 
                SqlCommand com = new SqlCommand("SELECT " + fields + " FROM " + table + " " + WHERE, connection);

                //foreach key, add parammeter
                foreach (var key in parameters.Keys)
                {
                    //com.Parameters.Add(key, SqlDbType.NVarChar).Value = parameters[key];
                    com.Parameters.AddWithValue(key, parameters[key]);
                }

                connection.Open();
                //' execute the select statment 
                SqlDataReader result = com.ExecuteReader();
                var dt = new DataTable();
                dt.Load(result);

                return dt;
            }
        }
        public static DataTable SelectQueryFromSQLTable(string ConnectionStringName, string query, Dictionary<string, object> parameters)
        {
            //If theres sth empty
            if (string.IsNullOrEmpty(ConnectionStringName) || string.IsNullOrEmpty(query))
                return null;

            // read the coonection string from web.config 
            //Conexión SQL UPDATE USER
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString))
            {
                //' declare the command that will be used to execute the select statement 
                SqlCommand com = new SqlCommand(query, connection);

                //foreach key, add parammeter
                foreach (var key in parameters.Keys)
                {
                    //com.Parameters.Add(key, SqlDbType.NVarChar).Value = parameters[key];
                    com.Parameters.AddWithValue(key, parameters[key]);
                }

                connection.Open();
                //' execute the select statment 
                SqlDataReader result = com.ExecuteReader();
                var dt = new DataTable();
                dt.Load(result);

                return dt;
            }
        }

        public static void UpdateFromSQLTable(string ConnectionStringName, string table, string setClause, string whereClause, Dictionary<string, object> parameters)
        {
            //If theres sth empty
            if (string.IsNullOrEmpty(ConnectionStringName) || string.IsNullOrEmpty(setClause) || string.IsNullOrEmpty(table))
                return;

            //If theres where, add WHERE clause
            var WHERE = (string.IsNullOrEmpty(whereClause)) ? ("") : ("WHERE " + whereClause);
            // read the coonection string from web.config 
            //Conexión SQL UPDATE USER
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString))
            {
                //Setting SQL command and getting connection
                SqlCommand cmd = new SqlCommand("UPDATE " + table + " SET " + setClause + " " + WHERE);
                cmd.CommandType = CommandType.Text;
                cmd.Connection = connection;

                //foreach key, add parammeter
                foreach (var key in parameters.Keys)
                {
                    cmd.Parameters.AddWithValue(key, parameters[key]);
                }

                //Execute!
                connection.Open();
                cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteFromSQLTable(string ConnectionStringName, string table, string whereClause, Dictionary<string, object> parameters)
        {
            //If theres sth empty
            if (string.IsNullOrEmpty(ConnectionStringName) || string.IsNullOrEmpty(whereClause) || string.IsNullOrEmpty(table) || parameters == null)
                return;

            //add WHERE clause
            var WHERE = "WHERE " + whereClause;
            // read the connection string from web.config 
            //Conexión SQL DELETE
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings[ConnectionStringName].ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand("DELETE FROM " + table + " " + WHERE))
                {
                    //foreach key, add parammeter
                    foreach (var key in parameters.Keys)
                    {
                        cmd.Parameters.AddWithValue(key, parameters[key]);
                    }
                    cmd.Connection = connection;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }


        









        #region Utilities - Things with NOT SQL

        public static bool IsImage(HttpPostedFile postedFile)
        {
            //-------------------------------------------
            //  Check the image mime types
            //-------------------------------------------
            if (postedFile.ContentType.ToLower() != "image/jpg" &&
                    postedFile.ContentType.ToLower() != "image/jpeg" &&
                    postedFile.ContentType.ToLower() != "image/pjpeg" &&
                    postedFile.ContentType.ToLower() != "image/x-png" &&
                    postedFile.ContentType.ToLower() != "image/png")
            {
                return false;
            }

            //-------------------------------------------
            //  Check the image extension
            //-------------------------------------------
            if (Path.GetExtension(postedFile.FileName).ToLower() != ".jpg"
                && Path.GetExtension(postedFile.FileName).ToLower() != ".png"
                && Path.GetExtension(postedFile.FileName).ToLower() != ".jpeg")
            {
                return false;
            }
            return true;
        }

        //Configuración webconfig para acepta esta función:
        //<system.net>
        //    <mailSettings>
        //      <smtp deliveryMethod="Network">
        //        <network host="PC-Alejo.serversql.jeduca" port="25" enableSsl="false" defaultCredentials="false" />
        //      </smtp>
        //    </mailSettings>
        //  </system.net>
        public static void SendEmail(string sender, string recipient, string subject, string body)
        {
            MailMessage mm = new MailMessage(sender, recipient);
            mm.Subject = subject;
            mm.Body = body;
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Send(mm);
        }
        #endregion

    }
}

