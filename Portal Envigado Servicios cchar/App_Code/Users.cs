﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de Users
/// </summary>
public class Users
{
    public string UserName { get; set; }
    public string Contrasena { get; set; }
    public Users(string userName, string contrasena)
    {
        UserName = userName;
        Contrasena = contrasena;
    }
    //Other properties, methods, events...

}