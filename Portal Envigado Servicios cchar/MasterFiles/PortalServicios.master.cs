﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterFiles_PortalServicios : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(IsPostBack))
        {
            try
            {
                string[] filePaths = Directory.GetFiles(Server.MapPath("~/Images/"));
                List<ListItem> files = new List<ListItem>();
                foreach (string filePath in filePaths)
                {
                    if (Path.GetFileNameWithoutExtension(filePath) == "banner1")
                    {
                        imgBan1.ImageUrl = "/Images/" + Path.GetFileName(filePath);
                        imgBan11.ImageUrl = "/Images/" + Path.GetFileName(filePath);
                    }
                    if (Path.GetFileNameWithoutExtension(filePath) == "banner2")
                    {
                        imgBan2.ImageUrl = "/Images/" + Path.GetFileName(filePath);
                        imgBan22.ImageUrl = "/Images/" + Path.GetFileName(filePath);
                    }
                    if (Path.GetFileNameWithoutExtension(filePath) == "banner3")
                    {
                        imgBan3.ImageUrl = "/Images/" + Path.GetFileName(filePath);
                    }
                }
            }
            catch
            {

            }
        }

    }
}
