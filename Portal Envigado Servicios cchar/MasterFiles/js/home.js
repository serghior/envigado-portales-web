var contador_multimedia=0;//contador multimedia home
var contador_servicios=0;//contador servicios
var contador_multimedia_image=0;//contador multimedia home
//Current User
var currentUser =""; //variable para obtener el current user de sharepoint
var nombre_usuario=""; //obtener el nombre de usuario logueado



$(document).ready(function() {
		
		//HOME
		var url = window.location.href;
        var url_home = url.indexOf("Home.aspx");       
        if (url_home > -1){
        $("#captioned-gallery").show('fast');

        }
        else{
        $("#captioned-gallery").hide('fast');
        }
        
        $(function() {
    $("#cuadricula").tooltip({
      position: {
        my: "center bottom-20",
        at: "center top",
        using: function( position, feedback ) {
          $( this ).css( position );
          $( "<div>" )
            .addClass( "arrow" )
            .addClass( feedback.vertical )
            .addClass( feedback.horizontal )
            .appendTo( this );
        }
      }
    });
  });    
});

//CARGAR DESCRIPCION HOME
function CargarDescripcionHome(){
		$().SPServices({
		operation: "GetListItems",
		webURL:"/Admin/",
		CAMLQuery: "<Query><OrderBy><FieldRef Name='Created' Ascending='FALSE'/></OrderBy><RowLimit>1</RowLimit></Query>",		
		listName: "Home",
		
		completefunc: function (xData, Status) {
				$(xData.responseXML).SPFilterNode("z:row").each(function () {
				var Descripcion= $(this).attr("ows_Descripcion");	
				
				var decoded = decodeURI(Descripcion);
				$("#descripcionhome").append(Descripcion);
				});
		}
	});


}

//CARGAR ZONA MULTIMEDIA EN HOME
function CargarZonaMultimedia(){
	$().SPServices({
		operation: "GetListItems",
		webURL:"/Admin/",
		listName: "MultimediaHome",
		CAMLQuery: "<Query><Where><Eq><FieldRef Name='MultimediaEnHome'/><Value Type='Boolean'>1</Value></Eq></Where><OrderBy><FieldRef Name='Created' Ascending='FALSE'/></OrderBy><RowLimit>3</RowLimit></Query>",		
		//CAMLViewFields: "<ViewFields><FieldRef Name='Descripcion_Multimedia'/> <FieldRef Name='Url_Multimedia' /><FieldRef Name='MultimediaEnHome' /></ViewFields>",	
			completefunc: function (xData, Status) {
				$(xData.responseXML).SPFilterNode("z:row").each(function () {
				
				var Url = $(this).attr("ows_Url_Multimedia");
				var Descripcion= $(this).attr("ows_Descripcion_Multimedia");
				
				var url_video = Url.indexOf("youtube");
				
				var urlsplit = Url.split("=");
				var url_real = "https://www.youtube.com/embed/"+urlsplit[1];
				
					if (url_video > -1){
					contador_multimedia=contador_multimedia +1;
					
					$("#cont-multi").append("<div class='multi' id='multi"+contador_multimedia+"'></div>");
					$("#multi"+contador_multimedia+"").append("<iframe width='100%' height='150' src='"+url_real+"' frameborder='0' allowfullscreen></iframe><p id='desc_multim'>"+Descripcion+"</p>");		
					}
					else{
					contador_multimedia = contador_multimedia + 1;
					$("#cont-multi").append("<div class='contenedor' id='contenedor"+contador_multimedia+"'></div>");							
					$("#contenedor"+contador_multimedia+"").append("<a class='fancybox-button' rel='fancybox-button' href='"+Url+"' title='"+Descripcion+"'><img class='img-min' src='"+Url+"' alt='' /></a><p class='descrip-exterior'>"+Descripcion+"</p>");
					//$("#multi"+contador_multimedia+"").append("<p id='desc_multim'>"+Descripcion+"</p>");
					
					
					//$("#content_modal").append("<div id='openModal"+contador_multimedia+"' class='modalDialog'><a href='#close' title='Close' class='close'>X</a></div>");
					
					//$("#openModal"+contador_multimedia+"").append("<img src='"+Url+"'>");
					//$("#openModal"+contador_multimedia+"").append("<p id='desc_modal'>"+Descripcion+"</p>");
					}  
				
				});
			}
	});
}


//CARGAR QUIENES SOMOS EN PAGE QUIENES SOMOS
function CargarQuienesSomos(){

$().SPServices({
		operation: "GetListItems",
		listName: "QuienesSomos",
		webURL:"/Admin/",
		CAMLQuery: "<Query><Where><Eq><FieldRef Name='LinkTitle' /><Value Type='Computed'>Quienes Somos</Value></Eq></Where></Query>",		
		completefunc: function (xData, Status) {
				$(xData.responseXML).SPFilterNode("z:row").each(function () {
				
					var Item = $(this).attr("ows_Title");
					var TextoHtml = $(this).attr("ows_Texto");
				
					if (TextoHtml ==null)
					{
					}
					else
					{
					$("#content_qsomos").append("<div id='q_somos' class='quienes_somos'>"+TextoHtml+"</div>");
					}
				});
			}			
	});
}


//CARGAR MISION EN PAGE QUIENES SOMOS
function CargarMision(){

$().SPServices({
		operation: "GetListItems",
		webURL:"/Admin/",
		listName: "QuienesSomos",
		CAMLQuery: "<Query><Where><Eq><FieldRef Name='LinkTitle' /><Value Type='Computed'>Mision</Value></Eq></Where></Query>",		
		completefunc: function (xData, Status) {
				$(xData.responseXML).SPFilterNode("z:row").each(function () {
				
					var Item = $(this).attr("ows_Title");
					var TextoHtml = $(this).attr("ows_Texto");
				
					if (TextoHtml ==null)
					{
					}
					else
					{
					$("#content_qsomos").append("<div id='mision' class='quienes_somos'>"+TextoHtml+"</div>");
					}
				});
			}			
	});
}

//CARGAR VISION EN PAGE QUIENES SOMOS
function CargarVision(){

$().SPServices({
		operation: "GetListItems",
		webURL:"/Admin/",
		listName: "QuienesSomos",
		CAMLQuery: "<Query><Where><Eq><FieldRef Name='LinkTitle' /><Value Type='Computed'>Vision</Value></Eq></Where></Query>",		
		completefunc: function (xData, Status) {
				$(xData.responseXML).SPFilterNode("z:row").each(function () {
				
					var Item = $(this).attr("ows_Title");
					var TextoHtml = $(this).attr("ows_Texto");
				
					if (TextoHtml ==null)
					{
					}
					else
					{
					$("#content_qsomos").append("<div id='vision' class='quienes_somos'>"+TextoHtml+"</div>");
					}
				});
			}			
	});
}

//CARGAR VALORES EN PAGE QUIENES SOMOS
function CargarValores(){

$().SPServices({
		operation: "GetListItems",
		webURL:"/Admin/",
		listName: "QuienesSomos",
		CAMLQuery: "<Query><Where><Eq><FieldRef Name='LinkTitle' /><Value Type='Computed'>Valores</Value></Eq></Where></Query>",		
		completefunc: function (xData, Status) {
				$(xData.responseXML).SPFilterNode("z:row").each(function () {
				
					var Item = $(this).attr("ows_Title");
					var TextoHtml = $(this).attr("ows_Texto");
				
					if (TextoHtml ==null)
					{
					}
					else
					{
					$("#content_qsomos").append("<div id='valores' class='quienes_somos'>"+TextoHtml+"</div>");
					}
				});
			}			
	});
}

//CARGAR CASOS EN PAGE QUIENES SOMOS
function CargarCasos(){

$().SPServices({
		operation: "GetListItems",
		webURL:"/Admin/",
		listName: "QuienesSomos",
		CAMLQuery: "<Query><Where><Eq><FieldRef Name='LinkTitle' /><Value Type='Computed'>Casos de Exito</Value></Eq></Where></Query>",		
		completefunc: function (xData, Status) {
				$(xData.responseXML).SPFilterNode("z:row").each(function () {
				
					var Item = $(this).attr("ows_Title");
					var TextoHtml = $(this).attr("ows_Texto");
				
					if (TextoHtml ==null)
					{
					}
					else
					{
					$("#content_qsomos").append("<div id='casoexito' class='quienes_somos'>"+TextoHtml+"</div>");
					}
				});
			}			
	});
}

//CARGAR SERVICIOS EN PAGE SERVICIOS
function CargarServicios(){
		$().SPServices({
		operation: "GetListItems",
		webURL:"/Admin/",
		listName: "Servicios",
		CAMLQuery: "<Query><Where><Eq><FieldRef Name='Activo' /><Value Type='Boolean'>1</Value></Eq></Where><OrderBy><FieldRef Name='Created' Ascending='False' /></OrderBy></Query>",		
			completefunc: function (xData, Status) {
				$(xData.responseXML).SPFilterNode("z:row").each(function () {
				
				
				contador_servicios=contador_servicios +1;
				
				var descripcion =  $(this).attr("ows_Descripcion");
				var servi = $(this).attr("ows_Servicio");
				
				var url=$(this).attr("ows_Url");
				
				


//				$("#content_productos").append("<div title='"+servi+"' onclick='VistaPreviaServicio("+servicio+","+descripcion+","+ur+");' id='cuadrito'><img src='"+url+"'><figcaption>"+servi+"</figcaption><h2>"+servi+"</h2><p>"+descrip+"</p></div>");
	            $("#content_productos").append("<div class='producto'><a class='fancybox-button' rel='fancybox-button' href='"+url+"' title='"+descripcion+"'><img src='"+url+"' alt='' /><p class='nom-producto'>"+servi+"</p></a></div>");

//				if (contador_servicios==1){
//				$("#maxvist").append("<img src='"+url+"'><h2>"+servi+"</h2><p>"+descrip+"</p>");
//				}
																
				});
			}
	});

	}

//CARGAR VISTA PREVIA DE SERVICIOS
function VistaPreviaServicio(servicio, descripcion,ur){
	var servi=servicio;
	var descrip = descripcion;
	var url = ur;
	$("#maxvist").empty();
	$("#maxvist").append("<img src='"+url+"'><h2>"+servi+"</h2><p>"+descrip+"</p>");
}


//CARGAR ZONA MULTIMEDIA EN PAGE MULTIMEDIA
function CargarMultimedia(){

$().SPServices({
		operation: "GetListItems",
		webURL:"/Admin/",
		listName: "MultimediaHome",
		CAMLQuery: "<Query><OrderBy><FieldRef Name='Created' Ascending='False' /></OrderBy></Query>",		
			completefunc: function (xData, Status) {
				$(xData.responseXML).SPFilterNode("z:row").each(function () {
					var Url = $(this).attr("ows_Url_Multimedia");
					var Descripcion= $(this).attr("ows_Descripcion_Multimedia");
					var TipoMultimedia = $(this).attr("ows_TipoMultimedia");
					if (TipoMultimedia=="Video"){
					var urlposition = Url.split("=");
					var url_real = "https://www.youtube.com/embed/"+urlposition[1];
					$("#carousel").append("<li id='video-multimedia'><a href='#'><iframe width='100%' height='180.703' src='"+url_real+"' frameborder='0' allowfullscreen></iframe></a></li>");
					}
					else{
					contador_multimedia_image=contador_multimedia_image + 1;
				$("#carousel2").append("<li id='imagen-multimedia'><a class='fancybox-button' rel='fancybox-button' href='"+Url+"' title='"+Descripcion+"'><img class='img-min' src='"+Url+"' alt=''/></a></li>");
				//	$("#contenedor_sliders").append("<div id='openModal"+contador_multimedia_image+"' class='modalDialog'><a href='#close' title='Close' class='close'>X</a></div>");
				//	$("#openModal"+contador_multimedia_image+"").append("<img src='"+Url+"'>");
				//	$("#openModal"+contador_multimedia_image+"").append("<p id='desc_modal'>"+Descripcion+"</p>");
					}  
				});
				$( '#carousel2' ).elastislide();
				$( '#carousel' ).elastislide();	
			}		
	});
}

//CARGAR DATOS DE CONTACTO EN CONTACTENOS
function CargarDatosContacto(){

$().SPServices({
		operation: "GetListItems",
		webURL:"/Admin/",
		listName: "DatosContactenos",		
			completefunc: function (xData, Status) {
				$(xData.responseXML).SPFilterNode("z:row").each(function () {
					var Item = $(this).attr("ows_Item");
					var Informacion = $(this).attr("ows_Informacion");
					
					if (Item=="Direccion"){
					$("#lblDireccion").empty();	
					$("#lblDireccion").append(Informacion );
					}
					else if(Item=="Ciudad"){
					$("#lblCiudad").empty();
					$("#lblCiudad").append(Informacion );
					}
					else if(Item=="Telefono"){
					$("#lblTelefono").empty();
					$("#lblTelefono").append(Informacion );
					}
					else if(Item=="Movil"){
					$("#lblMovil").empty();
					$("#lblMovil").append(Informacion );
					}
					else if(Item=="Correo"){
					$("#Label1").empty();
					$("#Label1").append(Informacion );
					}
				});
			}		
	});

}

function RedireccionarPageAdmin(){

window.location.replace("/Paginas/HTML/index.html");	

}
